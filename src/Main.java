import model.Person;
import repository.BinaryTr;

public class Main {
    public static void main(String[] args) {
        BinaryTr bt = new BinaryTr();
        bt.add(new Person(1,"Andrew", "Pikozh", 23, "Kyiv"));
        bt.add(new Person(2, "Ivan", "Dorn", 35, "Kyiv"));
        bt.add(new Person(3,"Ivanov", "Korn", 37, "Kyiv" ));
        bt.add(new Person(4, "Nikolay", "Dorn", 35, "Kyiv"));
    }
}
