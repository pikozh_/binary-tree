package repository;

import model.Person;

public class BinaryTr implements BsTree {
    private Node root;
    private Node current;
    private int size;

    public static class Node {

        private String value;
        private Node left;
        private Node right;

        Node(String value) {
            this.value = value;
            right = null;
            left = null;
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void add(Person val) {
        root = addRecursive(val.getFirstName());
        size++;
    }

    @Override
    public void del(Person val) {
        root = deleteRecursive(root, val.getFirstName());
        size--;
    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight(Node root) {
        if (root == null) {
            return 0;
        } else {
            int left = getHeight(root.left);
            int right = getHeight(root.right);
            if (left > right) {
                return 1 + left;
            } else {
                return 1 + right;
            }
        }
    }
    @Override
    public void clear() {

    }

    @Override
    public Person[] toArray() {
        return new Person[0];
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }


    private Node addRecursive(String value) {
        if (current == null) {
            return new Node(value);
        }

        if (value.compareToIgnoreCase(current.value) < 0) {
            current.left = addRecursive(value);
        } else if (value.compareToIgnoreCase(current.value) > 0) {
            current.right = addRecursive(value);
        } else {
            return current;
        }
        return current;
    }

    private Node deleteRecursive(Node current, String value) {
        if (current == null) {
            return null;
        }

        if (value.equals(current.value)) {
            if (current.left == null && current.right == null) {
                return null;
            } else if (current.right == null) {
                return current.left;
            } else if (current.left == null) {
                return current.right;
            } else {
                String smallestValue = findSmallestValue(current.right);
                current.value = smallestValue;
                current.right = deleteRecursive(current.right, smallestValue);
                return current;
            }
        }
        if (value.compareToIgnoreCase(current.value) < 0) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }
        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private String findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }
}
