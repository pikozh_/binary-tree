package repository;

import model.Person;

public interface BsTree {

    void clear();

    int size();

    Person[] toArray();

    String toString();

    void add(Person val);

    void del(Person val);

    int getWidth();

    int getHeight(BinaryTr.Node root);

    int nodes();

    int leaves();
}
